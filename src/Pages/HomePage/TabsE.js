import React from "react";
import { Tabs } from "antd";
import { Card } from "antd";
import { Bounce } from "react-awesome-reveal";
const { Meta } = Card;
const onChange = (key) => {
  console.log(key);
};
function slowScroll(destination, duration) {
  let start = window.pageYOffset;
  let startTime = null;

  function animation(currentTime) {
    if (startTime === null) startTime = currentTime;
    let timeElapsed = currentTime - startTime;
    let run = easeInOutQuad(timeElapsed, start, destination, duration);
    window.scrollTo(0, run);
    if (timeElapsed < duration) requestAnimationFrame(animation);
  }

  function easeInOutQuad(t, b, c, d) {
    t /= d / 2;
    if (t < 1) return (c / 2) * t * t + b;
    t--;
    return (-c / 2) * (t * (t - 2) - 1) + b;
  }

  requestAnimationFrame(animation);
}
export default function TabsE() {
  return (
    <Bounce triggerOnce duration={2500} >
      <div className="py-20 w-screen shadow-xl shadow-red-200/50">
        <div
          className="flex justify-center items-center px-5 w-full lg:px-40 "
        />
        <h2 className="lg:text-3xl md:text-xl text-red-600 font-bold ">ĐIỂM VƯỢT TRỘI ELEARNING</h2>
        <div className="mt-10 text-left block grid-cols-3 lg:grid md:grid px-10 lg:px-56 ">
          <div className="col-span-2 flex flex-col justify-center lg:mr-8">
            <p className="text-3xl mb-5 font-medium text-red-600">
              Chúng tôi tin vào tiềm năng của con người
            </p>
            <p className="text-2xl">
              CyberSoft được thành lập dựa trên niềm tin rằng bất cứ ai cũng có
              thể học lập trình. <br /> Bất kể ai cũng có thể là một lập trình,
              tham gia trong đội ngữ Tech, bất kể tuổi tác, nền tảng, giới tính
              hoặc tình trạng tài chính. Chúng tôi không bỏ qua những người mới
              bắt đầu hoặc chưa có kinh nghiệm theo đuổi đam mê lập trình. Thay
              vào đó, chúng tôi chào đón học viên của tất cả các cấp độ kinh
              nghiệm. Lộ trình học tập của CyberSoft may đo cho từng đối tượng để
              học xong và đi làm ngay.
            </p>
            <button
              onClick={() => {
                window.location.href = "/listcourse";
              }}
              className="font-bold mt-5 px-1 py-2 w-40 border-2 border-red-500/75  flex justify-center items-center rounded text-white bg-red-500 hover:brightness-75"
            >
              Tham Khảo Khóa Học
            </button>
          </div>
          <img
            className="col-span-1 hidden lg:block md:block lg:ml-5"
            width={400}
            src={require("../../img/Elagi.jpg")}
            alt=""
          />
        </div>
      </div>
    </Bounce>
  );
}
