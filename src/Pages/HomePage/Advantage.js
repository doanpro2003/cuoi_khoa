import React from "react";
import {
  ClusterOutlined,
  DeploymentUnitOutlined,
  KeyOutlined,
  RedditOutlined,
  ToolOutlined,
  UsergroupAddOutlined,
} from "@ant-design/icons";
import { Bounce, Slide } from "react-awesome-reveal";
export default function Advantage() {
  return (
    <div className="px-10 my-10 lg:px-20 md:px20">
      <Bounce triggerOnce direction="left" duration={2000}>
        <div className="text-right mb-10">
          <h2 className="text-5xl tracking-widest font-bold text-center text-red-600">
            Giới thiệu
          </h2>
          <p className="text-2xl text-center text-red-600  ">ELEARNING</p>
        </div>
      </Bounce>
      <div className="block mx-10">
        <Slide duration={2060} direction="right" triggerOnce>
          <div className="mt-3 block text-center bold text-justify">
            <div className="col-span-4">
              <p className="mt-5 text-2xl">
                Elearning là một hình thức học tập khá phổ biến hiện nay, đặc biệt là ở giới trẻ. Đây là hình thức giáo dục trực tuyến thông qua mạng internet giúp người học dễ dàng tìm hiểu, tiếp nhận được những thông tin nhanh chóng, tiện lợi chỉ với thiết bị kết nối mạng như máy tính, điện thoại, máy tính bảng,… Bản chất của hình thức này chính là học tập từ xa, hoàn toàn không cần tới trường học, lớp học.

                Học tập trực tuyến là cách phân phối bài giảng, các tài liệu, những nội dung thông qua công cụ hỗ trợ tiên tiến và hiện đại. Nội dung được đăng tải lên website, qua phần mềm học trực tuyến,… để người học dễ dàng tham gia học tập. Không chỉ vậy, khả năng hỗ trợ tương tác giữa giáo viên và học viên cũng được đảm bảo tốt như yêu cầu.
              </p>
            </div>
          </div>
        </Slide>
      </div>
    </div>
  );
}
