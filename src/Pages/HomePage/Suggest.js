import { Button, Card, message } from "antd";
import React, { useEffect, useState } from "react";
import { Bounce } from "react-awesome-reveal";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { getCourse, postRegisCourse } from "./../../service/courseService";

export default function Suggest() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  const [Cours, setCours] = useState([]);
  useEffect(() => {
    getCourse()
      .then((res) => {
        // console.log(res);
        setCours(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderCardCours = () => {
    return Cours.slice(0, 8).map((item) => {
      let axiosArr = {
        maKhoaHoc: item.maKhoaHoc,
        taiKhoan: user?.taiKhoan,
      };
      return (
        <Card
          className="shadow-2xl my-10"
          hoverable
          cover={
            <img
              className="h-48 object-cover shadow-xl shadow-black-20/100"
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <p className="text-xl font-semibold tracking-tight text-gray-900 dark:text-white">
            {item.tenKhoaHoc}
          </p>

          <div className="">
            <div className="my-5">
              <p className="line-through italic">1.000.000đ</p>
              <p className="font-medium text-red-600 text-xl">800.000đ</p>
            </div>
            <div>
              <button
                onClick={() => {
                  window.location.href = `/detail/${item.maKhoaHoc}`;
                }}
                className="bg-red-500 mx-2 my-1 px-2 py-1 rounded-md text-white font-medium hover:brightness-75"
              >
                xem chi tiết
              </button>
              <button
                className="bg-red-500 mx-2 my-1 px-2 py-1 rounded-md text-white font-medium hover:brightness-75"
                onClick={() => {
                  postRegisCourse(axiosArr)
                    .then((res) => {
                      console.log(res);
                      message.success("Đăng ký thành công");
                    })
                    .catch((err) => {
                      console.log(err);
                      if (user == null) {
                        message.warning("vui lòng đăng nhập để thực hiện");
                      } else {
                        message.warning(err.request.response);
                      }
                    });
                }}
              >
                Đăng ký
              </button>
            </div>
          </div>
        </Card>
      );
    });
  };
  return (
    <div className="">
      <Bounce triggerOnce direction="right" duration={2000}>
        <div className="text-left">
          <p className="text-4xl tracking-widest font-bold text-center text-red-500 my-10 pl-20">
            KHÓA HỌC
          </p>
        </div>
      </Bounce>
      <div className="block  gap-10 px-20 lg:grid lg:grid-cols-4 md:grid md:grid-cols-3 md:px-5">
        {renderCardCours()}
      </div>

    </div>
  );
}
