import React from "react";
import Advantage from "./Advantage";
import Contact from "./Contact";
import Countup from "./Countup";
import SliderAnimate from "./SliderAnimate";
import Suggest from "./Suggest";
import TabsE from "./TabsE";
import backToTop from "reactjs-back-to-top";
import Loading from "../../Component/Loading/Loading";
export default function HomePage() {
  let option = {
    // text: "Lên Đầu Trang",
    // background: "#22c55e",
    // fontColor: "#000",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Top_Arrow.svg/1200px-Top_Arrow.svg.png",
    displayAfterScroll: 30, //percentage
    className: "", // custom class
  };
  backToTop.init(option);
  return (
    <div className="overflow-hidden">
      <SliderAnimate />
      <Advantage />
      <Countup />
      <TabsE />
      <Suggest />
      <Contact />
    </div>
  );
}
