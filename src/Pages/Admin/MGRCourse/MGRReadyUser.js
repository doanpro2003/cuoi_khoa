import React, { useEffect, useState } from "react";

import { message, Space, Table, Tag } from "antd";
import { postReadyUser } from "./../../../service/userService";
import { useParams } from "react-router-dom";
import { postDeleteCourse } from "../../../service/courseService";
export default function MGRReadyUser() {
  const columns = [
    {
      title: "tài khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Họ Tên",
      dataIndex: "hoTen",
      key: "hoTen",
    },
    {
      title: "Chờ Xác Nhận",
      dataIndex: "action",
      key: "action",
    },
  ];

  let param = useParams();
  const [readyUser, setreadyUser] = useState([]);
  const dataReadyUser = () => {
    return readyUser.map((item) => {
      return {
        ...item,
        action: (
          <div>
            <button
              onClick={() => {
                postDeleteCourse({
                  maKhoaHoc: param.tk,
                  taiKhoan: item.taiKhoan,
                })
                  .then((res) => {
                    message.success("hủy thành công");
                    setTimeout(() => {
                      window.location.reload();
                    }, 1000);
                    console.log(res);
                  })
                  .catch((err) => {
                    message.error("hủy thất bại");
                    console.log(err);
                  });
              }}
              className="border-2 border-black m-1 px-2 py-1 rounded-md"
            >
              hủy
            </button>
          </div>
        ),
      };
    });
  };
  useEffect(() => {
    postReadyUser({ maKhoaHoc: param.tk })
      .then((res) => {
        console.log(res);
        setreadyUser(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <p>HỌC VIÊN ĐÃ THAM GIA KHÓA HỌC</p>
      <Table columns={columns} dataSource={dataReadyUser()} />;
    </div>
  );
}
