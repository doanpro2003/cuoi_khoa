import React from "react";
import { Button, Form, Input, message, Select } from "antd";
import { postRegis } from "../../service/userService";
import Background from "../../img/anh_den.jpeg";
import { Flip, Slide } from "react-awesome-reveal";
import { useDispatch } from "react-redux";
import { setLoadingOff } from "../../redux-toolkit/loadingSlice";

const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: {
      // span: 24,
    },
    sm: {
      // span: 8,
    },
  },
  wrapperCol: {
    xs: {
      // span: 24,
    },
    sm: {
      // span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      // span: 24,
      // offset: 0,
    },
    sm: {
      // span: 16,
      // offset: 8,
    },
  },
};
export default function RegisPage() {
  let dispatch = useDispatch();
  dispatch(setLoadingOff());
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    postRegis(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng ký thành công");
        setTimeout(() => {
          window.location.href = "/login";
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error(err.request.responseText);
      });
  };

  return (
    <div className="  h-screen lg:grid lg:grid-cols-3 md:grid md:grid-cols-2">
      <Form
        className="px-5 my-auto py-5"
        layout="vertical"
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={onFinish}
        initialValues={{
          residence: ["zhejiang", "hangzhou", "xihu"],
          prefix: "86",
        }}
        scrollToFirstError
      >
        <p className="font-bold text-4xl text-red-500">ĐĂNG KÝ</p>

        <Form.Item
          name="taiKhoan"
          label={<p className="text-xs">TÀI KHOẢN</p>}
          rules={[
            {
              message: "Vui lòng nhập tài khoản!",
            },
          ]}
          hasFeedback
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="matKhau"
          label={<p className="text-xs">MẬT KHẨU</p>}
          rules={[
            {
              message: "Vui lòng nhập mật khẩu",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="hoTen"
          label={<p className="text-xs">HỌ TÊN</p>}
          rules={[
            {
              message: "vui lòng nhập họ tên",
              whitespace: true,
            },
          ]}
          hasFeedback
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="soDT"
          label={<p className="text-xs">SỐ ĐIỆN THOẠI</p>}
          rules={[
            {
              message: "vui lòng nhập số điện thoại",
            },
          ]}
          hasFeedback
        >
          <Input
            style={{
              width: "100%",
            }}
          />
        </Form.Item>
        <Form.Item
          name="maNhom"
          label={<p className="text-xs">MÃ NHÓM</p>}
          rules={[
            {
              message: "vui lòng nhập mã nhóm",
            },
          ]}
        >
          <Select placeholder="Chọn Mã nhóm">
            <Option value="GP11">GP11</Option>
          </Select>
        </Form.Item>

        <Form.Item
          name="email"
          label={<p className="text-xs">EMAIL</p>}
          rules={[
            {
              type: "email",
              message: "vui lòng nhập đúng định dạng email",
            },
            {
              message: "Please input your E-mail!",
            },
          ]}
          hasFeedback
        >
          <Input />
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          <button
            className="bg-red-500 text-white rounded-md px-5 py-2 hover:brightness-75"
            htmlType="submit"
          >
            Đăng Ký
          </button>
        </Form.Item>
      </Form>
      <div
        className="h-screen bg-center bg-cover text-white flex flex-col justify-center items-center py-10 md:col-span-1 lg:col-span-2"
        style={{ backgroundImage: `url(${Background})` }}
        height={"100%"}
      >
        <p className=" brightness-100 text-6xl font-semibold flex">
          <Flip triggerOnce duration={2000} direction="left" className="mr-3">
            <p>XIN </p>
          </Flip>
          <Flip triggerOnce duration={2000} direction="right">
            <p> CHÀO</p>
          </Flip>
          <Flip triggerOnce duration={2000} delay={1000} direction="down">
            <p>!</p>
          </Flip>
        </p>
        <p className="my-5 text-2xl">
          Nếu đã có tài khoản <br />
          Vui lòng đăng nhập để kết nối tài khoản của bạn.
        </p>
        <button
          onClick={() => {
            window.location.href = "/login";
          }}
          className="bg-red-500 py-2 px-4 rounded-md text-xl hover:brightness-75"
        >
          Đăng nhập
        </button>
      </div>
    </div>
  );
}
